//
// Created by Florian Contival on 22/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_CLIENTPARTICULIER_H
#define APPLICATIONGESTIONCOMPTECLIENTS_CLIENTPARTICULIER_H
#include <iostream>
#include "Client.h"

class ClientParticulier : public Client {
private:
    char situationFamiliale;
    std::string dateNaissance;

public:
    ClientParticulier(const std::string &nom="", const std::string &prenom="", char sexe='\0', std::string telephone=0, AdressePostale *adressePostale= nullptr, char situationFamiliale='\0', const std::string &dateNaissance="");

    virtual ~ClientParticulier();

    std::string getSituationFamiliale() const;
    void setSituationFamiliale(char situationFamiliale);

    const std::string &getDateNaissance() const;
    void setDateNaissance(const std::string &dateNaissance);

    void affiche() override;

};


#endif //APPLICATIONGESTIONCOMPTECLIENTS_CLIENTPARTICULIER_H

//
// Created by Florian Contival on 22/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_COMPTEBANCAIRE_H
#define APPLICATIONGESTIONCOMPTECLIENTS_COMPTEBANCAIRE_H
#include <iostream>
#include <vector>
#include "Operation.h"

class CompteBancaire {
private:
    static int last_numero;
    int numero;
    std::string dateOuverture;
    double solde;
    double montantDecouvertAutorise;
    int idClient;
    std::vector<Operation*> operations;

public:
    CompteBancaire(const std::string &dateOuverture="", double solde=0, double montantDecouvertAutorise=0, int idClient=-1);

    virtual ~CompteBancaire();

    int getNumero() const;

    const std::string &getDateOuverture() const;
    void setDateOuverture(const std::string &dateOuverture);

    double getSolde() const;
    void setSolde(double solde);

    double getMontantDecouvertAutorise() const;
    void setMontantDecouvertAutorise(double montantDecouvertAutorise);

    int getIdClient() const;
    void setIdClient(int idClient);

    const std::vector<Operation *> &getOperations() const;
    void afficher(std::string pad="");

    void ajouterOperation(std::string date, int code, float montant);

};


#endif //APPLICATIONGESTIONCOMPTECLIENTS_COMPTEBANCAIRE_H

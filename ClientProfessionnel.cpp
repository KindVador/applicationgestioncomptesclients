//
// Created by Florian Contival on 22/06/2020.
//

#include "ClientProfessionnel.h"
#include "InvalidUserInput.h"
#include <iomanip>
#include <chrono>
#include <ctime>

typedef std::chrono::system_clock Clock;

ClientProfessionnel::ClientProfessionnel(const std::string &nom, const std::string &prenom, char sexe, std::string telephone, AdressePostale *adressePostale, std::string siret, const std::string &raisonSociale, int anneeCreation,
                                         AdressePostale *adresseEntreprise, const std::string &adresseMail)
                                         : Client(nom, prenom, sexe, telephone, adressePostale), siret(siret), raisonSociale(raisonSociale), anneeCreation(anneeCreation), adresseEntreprise(adresseEntreprise), adresseMail(adresseMail) {}

ClientProfessionnel::~ClientProfessionnel() {
    std::cout << "Destruction du Client Professionnel : " << getNom() << ", " << getPrenom() << std::endl;
    delete adresseEntreprise;
}

std::string ClientProfessionnel::getSiret() const {
    return siret;
}

void ClientProfessionnel::setSiret(std::string siret) {
    if (siret.size() != 14) {
        throw InvalidUserInput("Le siret doit comporter 14 chiffres.");
    }
    ClientProfessionnel::siret = siret;
}

const std::string &ClientProfessionnel::getRaisonSociale() const {
    return raisonSociale;
}

void ClientProfessionnel::setRaisonSociale(const std::string &raisonSociale) {
    if (raisonSociale.size() > 50) {
        throw InvalidUserInput("La raison sociale doir avoir 50 caractères maximum.");
    }
    ClientProfessionnel::raisonSociale = raisonSociale;
}

int ClientProfessionnel::getAnneeCreation() const {
    return anneeCreation;
}

void ClientProfessionnel::setAnneeCreation(int anneeCreation) {
    if (std::to_string(anneeCreation).size() != 4) {
        throw InvalidUserInput("L'année de création doit être au format 4 digits (e.g. 2020).");
    }
    ClientProfessionnel::anneeCreation = anneeCreation;
}

AdressePostale *ClientProfessionnel::getAdresseEntreprise() const {
    return adresseEntreprise;
}

void ClientProfessionnel::setAdresseEntreprise(AdressePostale *adresseEntreprise) {
    ClientProfessionnel::adresseEntreprise = adresseEntreprise;
}

const std::string &ClientProfessionnel::getAdresseMail() const {
    return adresseMail;
}

void ClientProfessionnel::setAdresseMail(const std::string &adresseMail) {
    std::string::size_type n;
    n = adresseMail.find('@');
    if (n == std::string::npos) {
        throw InvalidUserInput("L'adresse mail doit contenir un @.");
    }
    ClientProfessionnel::adresseMail = adresseMail;
}

void ClientProfessionnel::affiche() {
    std::string pad = "    ";
    std::cout << "Professionnel : ";
    std::cout << std::setfill('0') << std::setw(5) << getId() << std::endl;
    std::cout << pad << "Siret : " << std::setfill('0') << std::setw(14) <<  getSiret() << std::endl;
    std::cout << pad << getRaisonSociale() << std::endl;
    std::time_t now_c = Clock::to_time_t(Clock::now());
    struct tm *parts = std::localtime(&now_c);
    int anciennete = (parts->tm_year + 1900) - getAnneeCreation();
    std::cout << pad << "Ancienneté : " << anciennete << std::endl;
    std::cout << getAdresseEntreprise()->toString(pad) << std::endl;
    std::cout << std::endl;

    if (getSexe() == 'M') {
        std::cout << pad << "M. ";
    } else {
        std::cout << pad  << "Mme ";
    }
    std::cout << getNom() << " " << getPrenom() << std::endl;
    std::cout << pad << "Téléphone : " << getTelephone() << std::endl;
    std::cout << pad << "Mail: " << getAdresseMail() << std::endl;
}

void ClientProfessionnel::ajouterAdresseEntreprise(const std::string &libelle, const std::string &complement, int codePostal, const std::string &ville) {
    this->adresseEntreprise = new AdressePostale(libelle, complement, codePostal, ville);
}

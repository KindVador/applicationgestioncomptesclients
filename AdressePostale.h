//
// Created by Florian Contival on 22/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_ADRESSEPOSTALE_H
#define APPLICATIONGESTIONCOMPTECLIENTS_ADRESSEPOSTALE_H
#include <iostream>
#include <sstream>

class AdressePostale {
private:
    std::string libelle;
    std::string complement;
    int codePostal;
    std::string ville;

public:
    AdressePostale(const std::string &libelle="", const std::string &complement="", int codePostal=0, const std::string &ville="");

    virtual ~AdressePostale();

    const std::string &getLibelle() const;
    void setLibelle(const std::string &libelle);

    const std::string &getComplement() const;
    void setComplement(const std::string &complement);

    int getCodePostal() const;
    void setCodePostal(int codePostal);

    const std::string &getVille() const;
    void setVille(const std::string &ville);

    std::string toString(std::string pad="");
};

std::ostream& operator<<(std::ostream& os, const AdressePostale& adresse);

#endif //APPLICATIONGESTIONCOMPTECLIENTS_ADRESSEPOSTALE_H

//
// Created by Florian Contival on 22/06/2020.
//

#include "CompteBancaire.h"
#include <iomanip>

int CompteBancaire::last_numero = 0;


CompteBancaire::CompteBancaire(const std::string &dateOuverture, double solde,
                               double montantDecouvertAutorise, int idClient) : dateOuverture(dateOuverture),
                                                                  solde(solde),
                                                                  montantDecouvertAutorise(montantDecouvertAutorise),
                                                                  idClient(idClient) {
    this->numero = last_numero++;
}

CompteBancaire::~CompteBancaire() {
    std::cout << "Destruction du Compte Bancaire N° : " << getNumero() << std::endl;
    for (auto op : operations) {
        delete op;
    }
}

int CompteBancaire::getNumero() const {
    return numero;
}

const std::string &CompteBancaire::getDateOuverture() const {
    return dateOuverture;
}

void CompteBancaire::setDateOuverture(const std::string &dateOuverture) {
    CompteBancaire::dateOuverture = dateOuverture;
}

double CompteBancaire::getSolde() const {
    return solde;
}

void CompteBancaire::setSolde(double solde) {
    CompteBancaire::solde = solde;
}

double CompteBancaire::getMontantDecouvertAutorise() const {
    return montantDecouvertAutorise;
}

void CompteBancaire::setMontantDecouvertAutorise(double montantDecouvertAutorise) {
    CompteBancaire::montantDecouvertAutorise = montantDecouvertAutorise;
}

int CompteBancaire::getIdClient() const {
    return idClient;
}

void CompteBancaire::setIdClient(int idClient) {
    CompteBancaire::idClient = idClient;
}

const std::vector<Operation *> &CompteBancaire::getOperations() const {
    return operations;
}

void CompteBancaire::afficher(std::string pad) {
    std::cout << pad << "Compte Numero : ";
    std::cout << pad << std::setfill('0') << std::setw(5) << getNumero() << std::endl;
    int opNb = 0;
    for(auto op : getOperations()) {
        std::cout << pad << "Mouvement " << ++opNb << " : ";
        op->afficher(pad);
    }
    std::cout << pad << "Solde : " << getSolde() << "Euros" << std::endl;
}

void CompteBancaire::ajouterOperation(std::string date, int code, float montant) {
    Operation* op = new Operation(date, code, montant);
    operations.push_back(op);
    // TODO surcharge operateur += et -=
    if (code == 3) {
        setSolde(getSolde() + montant);
    } else {
        setSolde(getSolde() - montant);
    }
}

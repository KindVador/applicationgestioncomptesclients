//
// Created by Florian Contival on 22/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_INVALIDUSERINPUT_H
#define APPLICATIONGESTIONCOMPTECLIENTS_INVALIDUSERINPUT_H
#include <iostream>
#include <exception>

class InvalidUserInput : public std::exception {
private:
    std::string msg;

public:
    InvalidUserInput(std::string c) throw() : msg(c) {}
    ~InvalidUserInput() throw() {}
    const char* what() const throw() { return msg.c_str(); }

};

#endif //APPLICATIONGESTIONCOMPTECLIENTS_INVALIDUSERINPUT_H

//
// Created by Florian Contival on 22/06/2020.
//

#include "AdressePostale.h"
#include <sstream>

AdressePostale::AdressePostale(const std::string &libelle, const std::string &complement, int codePostal,
                               const std::string &ville) : libelle(libelle), complement(complement),
                                                           codePostal(codePostal), ville(ville) {
    std::transform(this->ville.begin(), this->ville.end(), this->ville.begin(), ::toupper);
}

AdressePostale::~AdressePostale() {
    std::cout << "Destruction de l'adresse postale." << std::endl;
}

const std::string &AdressePostale::getLibelle() const {
    return libelle;
}

void AdressePostale::setLibelle(const std::string &libelle) {
    AdressePostale::libelle = libelle;
}

const std::string &AdressePostale::getComplement() const {
    return complement;
}

void AdressePostale::setComplement(const std::string &complement) {
    AdressePostale::complement = complement;
}

int AdressePostale::getCodePostal() const {
    return codePostal;
}

void AdressePostale::setCodePostal(int codePostal) {
    AdressePostale::codePostal = codePostal;
}

const std::string &AdressePostale::getVille() const {
    return ville;
}

void AdressePostale::setVille(const std::string &ville) {
    AdressePostale::ville = ville;
}

std::string AdressePostale::toString(std::string pad) {
    std::ostringstream oss;
    oss << pad << getLibelle() << "\n";
    if (!getComplement().empty()) {oss << pad << getComplement() << "\n";}
    oss << pad << std::to_string(getCodePostal()) << " " << getVille();
    return oss.str();
}

std::ostream& operator<<(std::ostream& os, const AdressePostale& adresse) {
    os << adresse.getLibelle() << std::endl;
    if (!adresse.getComplement().empty()) {os << adresse.getComplement() << std::endl;}
    os << std::to_string(adresse.getCodePostal()) << " " << adresse.getVille() << std::endl;
    return os;
}

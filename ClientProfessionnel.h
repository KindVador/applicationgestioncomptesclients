//
// Created by Florian Contival on 22/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_CLIENTPROFESSIONNEL_H
#define APPLICATIONGESTIONCOMPTECLIENTS_CLIENTPROFESSIONNEL_H
#include <iostream>
#include "Client.h"
#include "AdressePostale.h"

class ClientProfessionnel : public Client {
private:
    std::string siret;
    std::string raisonSociale;
    int anneeCreation;
    AdressePostale* adresseEntreprise;
    std::string adresseMail;

public:
    ClientProfessionnel(const std::string &nom="", const std::string &prenom="", char sexe='\0', std::string telephone=0, AdressePostale *adressePostale= nullptr, std::string siret="", const std::string &raisonSociale="", int anneeCreation=0,
                        AdressePostale *adresseEntreprise= nullptr, const std::string &adresseMail="");

    virtual ~ClientProfessionnel();

    std::string getSiret() const;
    void setSiret(std::string siret);

    const std::string &getRaisonSociale() const;
    void setRaisonSociale(const std::string &raisonSociale);

    int getAnneeCreation() const;
    void setAnneeCreation(int anneeCreation);

    AdressePostale *getAdresseEntreprise() const;
    void setAdresseEntreprise(AdressePostale *adresseEntreprise);

    const std::string &getAdresseMail() const;
    void setAdresseMail(const std::string &adresseMail);

    void affiche() override;
    void ajouterAdresseEntreprise(const std::string &libelle, const std::string &complement, int codePostal, const std::string &ville);
};

#endif //APPLICATIONGESTIONCOMPTECLIENTS_CLIENTPROFESSIONNEL_H

//
// Created by Florian Contival on 23/06/2020.
//

#include "Operation.h"

int Operation::last_id = 0;
std::map<int, std::string> Operation::types = {std::pair<int, std::string>(1, "Retrait DAB"),
                                               std::pair<int, std::string>(2, "Paiement Carte Bleue"),
                                               std::pair<int, std::string>(3, "Dépôt Guichet")};

Operation::Operation(const std::string &date, int code, float montant) : date(date), code(code), montant(montant) {
    this->id = last_id++;
}

Operation::~Operation() {

}

const std::string &Operation::getDate() const {
    return date;
}

void Operation::setDate(const std::string &date) {
    Operation::date = date;
}

int Operation::getCode() const {
    return code;
}

void Operation::setCode(int code) {
    Operation::code = code;
}

float Operation::getMontant() const {
    return montant;
}

void Operation::setMontant(float montant) {
    Operation::montant = montant;
}

int Operation::getId() const {
    return id;
}

void Operation::afficher(std::string pad) {
    std::cout << pad << types.at(getCode()) << " de " << getMontant() << " Euros" << std::endl;
}

double operator+(const Operation &a, const Operation &b) {
    return a.getMontant() + b.getMontant();
}

double operator-(const Operation &a, const Operation &b) {
    return a.getMontant() - b.getMontant();
}

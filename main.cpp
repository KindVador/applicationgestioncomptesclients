//
// Created by Florian Contival on 22/06/2020.
//
#include <cstdlib>
#include <iostream>
#include <map>
#include <algorithm>
#include <fstream>
#include <string>
#include "ClientParticulier.h"
#include "ClientProfessionnel.h"
#include "InvalidUserInput.h"
#define DEBUG 1

using namespace std;

void ClearConsole()
{
#if defined _WIN32
    system("cls");
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif
}

int findClientForCompteId(map<int, Client*> &clients, int compteId) {
    for (auto c : clients) {
        for (auto cpt : c.second->getComptes()) {
            if (cpt->getNumero() == compteId) {
                return c.second->getId();
            }
        }
    }
    return -1;
}

AdressePostale* SaisirAdresse() {
    // TODO move this function as static in the class AdressePostale
    AdressePostale* a = nullptr;
    string libelle="", complement="", ville="";
    int codePostal=0;
    try {
        cin.clear();
        fflush(stdin);
        cout <<  "ADRESSE: Saisir le libelle : " << endl;
        getline(cin, libelle);
        cin.clear();
        fflush(stdin);
        cout <<  "ADRESSE: Saisir le complement : " << endl;
        getline(cin, complement);
        cin.clear();
        fflush(stdin);
        cout <<  "ADRESSE: Saisir le code postal : " << endl;
        cin >> codePostal;
        cout <<  "ADRESSE: Saisir la ville : " << endl;
        cin >> ville;
    } catch (InvalidUserInput &ex) {
        cerr << "ERREUR : " << ex.what() << endl;
        return nullptr;
    }
    a = new AdressePostale(libelle, complement, codePostal, ville);
    return a;
}

ClientParticulier* SaisirClientParticulier() {
    // TODO move this function as static in the class ClientParticulier
    ClientParticulier* cp = nullptr;
    string nom = "", prenom = "", telephone = "", dateNaissance = "";
    char sexe = '\0', situationFamiliale = '\0';
    AdressePostale* adresse = nullptr;
    try {
        cout <<  "Saisir le nom : " << endl;
        cin >> nom;
        cout <<  "Saisir le prénom : " << endl;
        cin >> prenom;
        cout <<  "Saisir le sexe: F ou M " << endl;
        cin >> sexe;
        cout <<  "Saisir le numéro de téléphone : " << endl;
        cin >> telephone;
        adresse = SaisirAdresse();
        cout <<  "Saisir la situation familiale: C, M, D ou X : " << endl;
        cin >> situationFamiliale;
        cout <<  "Saisir la date de naissance au format jj/mm/aaa: " << endl;
        cin >> dateNaissance;
    } catch (InvalidUserInput &ex) {
        cerr << "ERREUR : " << ex.what() << endl;
        return nullptr;
    }
    cp = new ClientParticulier(nom, prenom, sexe, telephone, adresse, situationFamiliale, dateNaissance);
    return cp;
}

ClientProfessionnel* SaisirClientProfessionnel() {
    // TODO move this function as static in the class ClientProfessionnel
    ClientProfessionnel* cp = nullptr;
    string nom = "", prenom = "", telephone = "", siret = "", raisonSociale= "", adresseMail="";
    char sexe = '\0';
    AdressePostale* adresse = nullptr;
    int anneeCreation=0;
    try {
        cout <<  "Saisir le nom : " << endl;
        cin >> nom;
        cout <<  "Saisir le prénom : " << endl;
        cin >> prenom;
        cout <<  "Saisir le sexe: F ou M " << endl;
        cin >> sexe;
        cout <<  "Saisir le numéro de téléphone : " << endl;
        cin >> telephone;
        cout <<  "Saisir le numero de siret (14 chiffres): " << endl;
        cin >> siret;
        cin.clear();
        fflush(stdin);
        cout <<  "Saisir la raison sociale: " << endl;
        getline(cin, raisonSociale);
        cin.clear();
        fflush(stdin);
        cout <<  "Saisir l'annee de création (4 digits): " << endl;
        cin >> anneeCreation;
        adresse = SaisirAdresse();
        cout <<  "Saisir l'adress mail: " << endl;
        cin >> adresseMail;
    } catch (InvalidUserInput &ex) {
        cerr << "ERREUR : " << ex.what() << endl;
        return nullptr;
    }
    cp = new ClientProfessionnel(nom, prenom, sexe, telephone, nullptr, siret, raisonSociale, anneeCreation, adresse, adresseMail);
    return cp;
}

int Menu3(map<int, Client*> &clients) {
    int userChoice = 0;
    cout << "MENU Ajouter/Supprimer/Modifier un Client : " << endl;
    do {
        cout << "Choisir une option dans le menu ci-dessous:" << endl;
        cout << "1 - Ajouter un client" << endl;
        cout << "2 - Supprimer un client (NON REALISE)" << endl;
        cout << "3 - Modifier un client (NON REALISE)" << endl;
        cout << "4 - Retour MENU PRINCIPAL" << endl;
        cin >> userChoice;
    } while (!(userChoice >= 1 && userChoice <= 4));

    switch (userChoice) {
        case 1:
            {
                int userChoice2 = 0;
                do {
                    cout << "Saisir 1 pour un Particulier ou 2 pour un Professionnel : " << endl;
                    cin >> userChoice2;
                    if (userChoice2 == 1) {
                        ClientParticulier* cp = SaisirClientParticulier();
                        clients[cp->getId()] = cp;
                    } else {
                        if (userChoice2 == 2) {
                            ClientProfessionnel* cp = SaisirClientProfessionnel();
                            clients[cp->getId()] = cp;
                        }
                    }
                } while (!(userChoice2 == 1 || userChoice2 == 2));
            }
            break;
        case 2:
            {

            }
            break;
        case 3:
            {

            }
            break;
        case 4:
            return 0;
        default:
            cout << "ERREUR choix d'option inconnu pour " << userChoice << endl;
            break;
    }

    return 1;
}

int Menu4(map<int, Client*> &clients) {
    return 1;
}

void ImporterFichierOperations(map<int, Client*> &clients, string fileName="Operations.txt", string delimiter=";") {
    ifstream file(fileName);
    ofstream fileAnomalies("Anomalies.log");
    if (!file.is_open()) {
        cerr << "Erreur: impossible d'ouvrir le fichier " << fileName << endl;
    } else if (!fileAnomalies.is_open()) {
        cerr << "Erreur: impossible d'ouvrir le fichier Anomalies.log" << endl;
    } else {
        string line;
        int idClient = -1, previousNumeroCompte = -1;
        while (std::getline(file, line)) {
            size_t pos = 0;
            string token;
            int attributPos = 0;
            int numeroCompte=0, codeOperation=0;
            string dateOperation="";
            float montantOperation=0.0;
            while ((pos = line.find(delimiter)) != string::npos) {
                token = line.substr(0, pos);
                switch (attributPos) {
                    case 0:
                        numeroCompte = stoi(token);
                        attributPos++;
                        break;
                    case 1:
                        dateOperation = token;
                        attributPos++;
                        break;
                    case 2:
                        codeOperation = stoi(token);
                        attributPos = 0;
                        break;
                }
                line.erase(0, pos + delimiter.length());
            }
            montantOperation = stof(line);
            if (codeOperation != 1 && codeOperation != 2 && codeOperation != 3) {
                stringstream ss;
                ss << numeroCompte << delimiter << dateOperation << delimiter << codeOperation << delimiter << montantOperation << "\n";
                fileAnomalies << ss.str();
            } else if (numeroCompte != previousNumeroCompte) {
                idClient = findClientForCompteId(clients, numeroCompte);
                previousNumeroCompte = numeroCompte;
            }
            if (idClient != -1) {
                clients.at(idClient)->ajouterOperationAuCompte(numeroCompte, dateOperation, codeOperation, montantOperation);
            }
        }
        file.close();
        fileAnomalies.close();
    }
}

int MenuPrincipal(map<int, Client*> &clients) {
    int userChoice = 0;
//    ClearConsole();
    cout << "MENU PRICIPAL: " << endl;
    do {
        cout << "Choisir une option dans le menu ci-dessous:" << endl;
        cout << "1 - Lister l’ensemble des clients" << endl;
        cout << "2 - Consulter les soldes des comptes pour un client donné" << endl;
        cout << "3 - Ajouter/Supprimer/Modifier un Client" << endl;
        cout << "4 - Ajouter/Supprimer/Modifier une Opération (NON REALISE)" << endl;
        cout << "5 - Afficher l’ensemble des opérations pour un compte donné" << endl;
        cout << "6 - Importer le fichier des Opérations Bancaires" << endl;
        cout << "7 - Quitter" << endl;
        cin >> userChoice;

    } while (!(userChoice >= 1 && userChoice <= 7));

    switch (userChoice) {
        case 1:
            ClearConsole();
            for(auto c : clients) {
                c.second->affiche();
            }
            break;
        case 2:
            {
                ClearConsole();
                int idClient=-1;
                cout << "Entrez un numéro de client :" << endl;
                cin >> idClient;
                auto search = clients.find(idClient);
                if (search == clients.end()) {
                    cout << "Erreur: Mauvais numero client" << endl;
                } else {
                    clients.at(idClient)->afficherSoldeDesComptes();
                }
            }
            break;
        case 3:
            do {
                userChoice = Menu3(clients);
            } while (userChoice != 0);
            break;
        case 4:
            do {
                userChoice = Menu4(clients);
            } while (userChoice != 0);
            break;
        case 5:
            {
                ClearConsole();
                int idCompte=-1, idClient=-1;
                std::cout << "Entrez un numéro de compte :" << std::endl;
                std::cin >> idCompte;
                idClient = findClientForCompteId(clients, idCompte);
                if (idClient == -1) {
                    std::cout << "Erreur: Mauvais numero de compte" << std::endl;
                } else {
                    clients.at(idClient)->afficherCompte(idCompte);
                }
            }
            break;
        case 6:
            ImporterFichierOperations(clients, "Operations.txt");
            break;
        case 7:
            return 0;
        default:
            cout << "ERREUR choix d'option inconnu pour " << userChoice << endl;
            break;
    }

    return 1;
}

int main() {
    // DECLARATIONS & INITIALISATIONS
    int status = 1;
    map<int, Client*> clients;

    // Ajout de clients et compte dans la map
    if(DEBUG) {
        ClientParticulier* c1 = new ClientParticulier("bety", "daniel", 'M', "0602010503", nullptr, 'C', "27/07/1983");
        c1->ajouterAdressePostale("12, rue des Oliviers", "", 94000, "creteil");
        c1->creerCompte("01/01/2020", 1247.54, 500);
        c1->creerCompte("22/06/2020", 2478.45, 750);
        CompteBancaire* cb1 = new CompteBancaire();
        c1->ajouterOperationAuCompte(0, "23/06/2020", 1, 100);
        c1->ajouterOperationAuCompte(0, "24/06/2020", 2, 150);
        c1->ajouterOperationAuCompte(0, "25/06/2020", 3, 50);
        c1->ajouterOperationAuCompte(1, "26/06/2020", 1, 100);
        c1->ajouterOperationAuCompte(1, "27/06/2020", 2, 150);
        c1->ajouterOperationAuCompte(1, "28/06/2020", 3, 50);
        clients[c1->getId()] = c1;

        ClientProfessionnel* c2 = new ClientProfessionnel("bougha", "céline", 'F', "0145010503", nullptr, "02123654545555", "KONAMI", 2015,
                                             nullptr, "c.bougha@gmail.com");
        c2->ajouterAdresseEntreprise("3, rue la République", "", 77680, "roissy-en-brie");
        c2->creerCompte("02/02/2019", 15000.00, 1500);
        c2->creerCompte("23/06/2020", 12345.67, 2000);
        c2->ajouterOperationAuCompte(2, "29/06/2020", 1, 100);
        c2->ajouterOperationAuCompte(2, "30/06/2020", 2, 150);
        c2->ajouterOperationAuCompte(2, "01/07/2020", 3, 50);
        c2->ajouterOperationAuCompte(3, "02/07/2020", 1, 100);
        c2->ajouterOperationAuCompte(3, "03/07/2020", 2, 150);
        c2->ajouterOperationAuCompte(3, "04/07/2020", 3, 50);
        clients[c2->getId()] = c2;
    }

    // CORPS DU PROGRAMME
    cout << "=====================================================" << endl;
    cout << " Bienvenue dans l'application de gestion des comptes " << endl;
    cout << "=====================================================" << endl;

    while(status)
    {
        status = MenuPrincipal(clients);
    }

    cout << "=====================================================" << endl;
    cout << "                     A bientot " << endl;
    cout << "=====================================================" << endl;

    // destruction des clients
    for (auto c : clients) {
        delete c.second;
    }

    return 0;
}
//
// Created by Florian Contival on 23/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_OPERATION_H
#define APPLICATIONGESTIONCOMPTECLIENTS_OPERATION_H
#include <iostream>
#include <string>
#include <map>

class Operation {
private:
    static int last_id;
    static std::map<int, std::string> types;
    int id;
    std::string date;
    int code;
    float montant;

public:
    Operation(const std::string &date="", int code=9, float montant=0.0);

    virtual ~Operation();

    int getId() const;

    const std::string &getDate() const;
    void setDate(const std::string &date);

    int getCode() const;
    void setCode(int code);

    float getMontant() const;
    void setMontant(float montant);

    void afficher(std::string pad="");

    friend double operator+(const Operation &a, const Operation &b);
    friend double operator-(const Operation &a, const Operation &b);
};

#endif //APPLICATIONGESTIONCOMPTECLIENTS_OPERATION_H

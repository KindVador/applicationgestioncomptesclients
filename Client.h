//
// Created by Florian Contival on 22/06/2020.
//

#ifndef APPLICATIONGESTIONCOMPTECLIENTS_CLIENT_H
#define APPLICATIONGESTIONCOMPTECLIENTS_CLIENT_H
#include <iostream>
#include <vector>
#include "AdressePostale.h"
#include "CompteBancaire.h"

class Client {
private:
    static int last_client_id;
    int id;
    std::string nom;
    std::string prenom;
    char sexe;
    std::string telephone;
    AdressePostale* adressePostale;
    std::vector<CompteBancaire*> comptes;

public:
    Client(const std::string &nom="", const std::string &prenom="", char sexe='\0', std::string telephone="", AdressePostale *adressePostale= nullptr);

    virtual ~Client();

    static int getLastClientId();
    static void setLastClientId(int lastClientId);

    int getId() const;

    const std::string &getNom() const;
    void setNom(const std::string &nom);

    const std::string &getPrenom() const;
    void setPrenom(const std::string &prenom);

    char getSexe() const;
    void setSexe(char sexe);

    std::string getTelephone() const;
    void setTelephone(std::string telephone);

    AdressePostale *getAdressePostale() const;
    void setAdressePostale(AdressePostale *adressePostale);

    const std::vector<CompteBancaire *> &getComptes() const;
    void setComptes(const std::vector<CompteBancaire *> &comptes);

    virtual void affiche() = 0;

    virtual void ajouterAdressePostale(const std::string &libelle, const std::string &complement, int codePostal, const std::string &ville);
    virtual void ajouterCompte(CompteBancaire* cb);
    virtual void creerCompte(const std::string &dateOuverture, double solde, double montantDecouvertAutorise);
    virtual void afficherSoldeDesComptes();
    virtual void afficherCompte(int compteId);
    virtual CompteBancaire* getCompteForId(int compteId);
    virtual void ajouterOperationAuCompte(int compteId, std::string date, int code, float montant);

};


#endif //APPLICATIONGESTIONCOMPTECLIENTS_CLIENT_H

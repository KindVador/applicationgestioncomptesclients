//
// Created by Florian Contival on 22/06/2020.
//

#include "ClientParticulier.h"
#include "InvalidUserInput.h"
#include <iomanip>
#include <chrono>
#include <ctime>

typedef std::chrono::system_clock Clock;

ClientParticulier::ClientParticulier(const std::string &nom, const std::string &prenom, char sexe, std::string telephone, AdressePostale *adressePostale, char situationFamiliale, const std::string &dateNaissance)
: Client(nom, prenom, sexe, telephone, adressePostale), situationFamiliale(situationFamiliale), dateNaissance(dateNaissance) {}

ClientParticulier::~ClientParticulier() {
    std::cout << "Destruction du Client Particulier : " << getNom() << ", " << getPrenom() << std::endl;
}

std::string ClientParticulier::getSituationFamiliale() const {
    switch (situationFamiliale) {
        case 'C':
            return "Célibataire";
        case 'M':
            return "Marié(e)";
        case 'D':
            return "Divorcé(e)";
        default:
            return "Autre";
    }

}

void ClientParticulier::setSituationFamiliale(char situationFamiliale) {
    if (!(situationFamiliale == 'C' || situationFamiliale == 'M' || situationFamiliale == 'D' || situationFamiliale == 'X')) {
        throw InvalidUserInput("Le situation familiale du client doit être C, M, D ou X.");
    }
    ClientParticulier::situationFamiliale = situationFamiliale;
}

const std::string &ClientParticulier::getDateNaissance() const {
    return dateNaissance;
}

void ClientParticulier::setDateNaissance(const std::string &dateNaissance) {
    ClientParticulier::dateNaissance = dateNaissance;
}

void ClientParticulier::affiche() {
    std::string pad = "    ";
    std::cout << "Particulier : ";
    std::cout << std::setfill('0') << std::setw(5) << getId() << std::endl;
    if (getSexe() == 'M') {
        std::cout << pad << "M. ";
    } else {
        std::cout << pad  << "Mme ";
    }
    std::cout << getNom() << " " << getPrenom() << std::endl;
    std::cout << getAdressePostale()->toString(pad);
    std::cout << std::endl << std::endl;
    std::cout << pad << "Téléphone : " << getTelephone() << std::endl;
    std::cout << pad << "Situation Familiale : " << getSituationFamiliale() << std::endl;
    std::time_t now_c = Clock::to_time_t(Clock::now());
    struct tm *parts = std::localtime(&now_c);
    int age = (parts->tm_year + 1900) - std::stoi(getDateNaissance().substr(6, 4));
    std::cout << pad << "Age : " << age << std::endl;
}

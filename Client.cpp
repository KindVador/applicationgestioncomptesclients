//
// Created by Florian Contival on 22/06/2020.
//

#include "Client.h"
#include "InvalidUserInput.h"
#include <algorithm>
#include <string>
#include <cctype>

int Client::last_client_id = 0;

Client::Client(const std::string &nom, const std::string &prenom, char sexe, std::string telephone,
               AdressePostale *adressePostale) : sexe(sexe), telephone(telephone) {
    this->id = last_client_id++;
    this->setNom(nom);
    this->setPrenom(prenom);
    this->setAdressePostale(adressePostale);
}

Client::~Client() {
    std::cout << "Destruction du client : " << getNom() << ", " << getPrenom() << std::endl;
    delete adressePostale;
    for (auto cpt : comptes) {
        delete cpt;
    }
}

const std::string &Client::getNom() const {
    return nom;
}

void Client::setNom(const std::string &nom) {
    if(nom.size() == 0 || nom.size() > 50) {
        throw InvalidUserInput("Le nom du client doit avoir entre 1 et 50 caractères au lieu de " + std::to_string(nom.size()) + " caractères.");
    }
    Client::nom = nom;
    std::transform(Client::nom.begin(), Client::nom.end(), Client::nom.begin(), ::toupper);
}

const std::string &Client::getPrenom() const {
    return prenom;
}

void Client::setPrenom(const std::string &prenom) {
    if(prenom.size() == 0 || prenom.size() > 50) {
        throw InvalidUserInput("Le prénom du client doit avoir entre 1 et 50 caractères au lieu de " + std::to_string(prenom.size()) + " caractères.");
    }
    Client::prenom = prenom;
    std::transform(Client::prenom.begin(), Client::prenom.end(), Client::prenom.begin(), ::tolower);
    Client::prenom[0] = toupper(Client::prenom[0]);
}

char Client::getSexe() const {
    return sexe;
}

void Client::setSexe(char sexe) {
    if(!(sexe == 'F' || sexe == 'M')) {
        throw InvalidUserInput("Le sexe doit être F ou M et non : " + std::to_string(sexe) + ".");
    }
    Client::sexe = sexe;
}

std::string Client::getTelephone() const {
    return telephone;
}

void Client::setTelephone(std::string telephone) {
    if ((telephone.size() != 10)) {
        throw InvalidUserInput("Le numéro de téléphone doit avoir 10 chiffres.");
    }
    Client::telephone = telephone;
}

AdressePostale *Client::getAdressePostale() const {
    return adressePostale;
}

void Client::setAdressePostale(AdressePostale *adressePostale) {
    Client::adressePostale = adressePostale;
}

const std::vector<CompteBancaire *> &Client::getComptes() const {
    return comptes;
}

void Client::setComptes(const std::vector<CompteBancaire *> &comptes) {
    Client::comptes = comptes;
}

int Client::getLastClientId() {
    return last_client_id;
}

void Client::setLastClientId(int lastClientId) {
    last_client_id = lastClientId;
}

int Client::getId() const {
    return id;
}

void Client::ajouterAdressePostale(const std::string &libelle, const std::string &complement, int codePostal,
                                   const std::string &ville) {
    this->adressePostale = new AdressePostale(libelle, complement, codePostal, ville);
}

void Client::ajouterCompte(CompteBancaire *cb) {
    comptes.push_back(cb);
}

void Client::creerCompte(const std::string &dateOuverture, double solde, double montantDecouvertAutorise) {
    CompteBancaire* cb = new CompteBancaire(dateOuverture, solde, montantDecouvertAutorise, getId());
    comptes.push_back(cb);
}

void Client::afficherSoldeDesComptes() {
    for (auto c : comptes) {
        std::cout << "Solde du Compte " << c->getNumero() << " est : " << c->getSolde() << std::endl;
    }
}

void Client::afficherCompte(int compteId) {
    std::string pad = "    ";
    std::cout << pad << "Titulaire : ";
    if (getSexe() == 'M') {
        std::cout << pad << "M. ";
    } else {
        std::cout << pad  << "Mme ";
    }
    std::cout << getNom() << " " << getPrenom() << std::endl;
    std::cout << std::endl;
    CompteBancaire* cb = getCompteForId(compteId);
    if (cb != nullptr) {
        cb->afficher(pad);
    }
}

CompteBancaire *Client::getCompteForId(int compteId) {
    for (auto cpt : getComptes()) {
        if (cpt->getNumero() == compteId) {
            return cpt;
        }
    }
    return nullptr;
}

void Client::ajouterOperationAuCompte(int compteId, std::string date, int code, float montant) {
    CompteBancaire* cb = getCompteForId(compteId);
    if (cb != nullptr) {
        cb->ajouterOperation(date, code, montant);
    }
}
